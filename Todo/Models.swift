//
//  Modles.swift
//  Todo
//
//  Created by Chris Brakebill on 1/11/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import RealmSwift

class Todo : Object {
    @objc dynamic var name: String = ""
    @objc dynamic var complete: Bool = false
}

class TodoList: Object {
    @objc dynamic var name: String = ""
    let todos = List<Todo>()
}
