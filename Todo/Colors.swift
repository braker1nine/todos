//
//  Colors.swift
//  Todo
//
//  Created by Chris Brakebill on 1/12/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import UIKit

extension UIColor {
    static let background = UIColor(red: 0.988, green: 0.985, blue: 0.972, alpha: 1.00)
    static let blue = UIColor(red: 0.316, green: 0.744, blue: 0.878, alpha: 1.00)
    static let lightGray = UIColor(red: 0.647, green: 0.647, blue: 0.68627451, alpha: 1.0)
    static let darkGray = UIColor(red: 0.301960784, green: 0.301960784, blue: 0.317647059, alpha: 1.0)
}
