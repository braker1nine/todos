//
//  AppDelegate.swift
//  Todo
//
//  Created by Chris Brakebill on 1/11/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

let realm = try! Realm()

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        let listsViewController = ListsViewController()
        let navigationController = UINavigationController(rootViewController: listsViewController)
        navigationController.navigationBar.barTintColor = .blue
        navigationController.navigationBar.tintColor = .white
        navigationController.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
        return true
    }
}

