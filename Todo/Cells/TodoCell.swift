//
//  TodoCell.swift
//  Todo
//
//  Created by Chris Brakebill on 1/12/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import AIFlatSwitch

class TodoCell : RoundedCell {
    
    struct ViewModel {
        var complete: Bool {
            return self.todo.complete
        }
        var name: String {
            return self.todo.name
        }
        
        private let todo: Todo
        
        init(todo: Todo) {
            self.todo = todo
        }
        
        func set(complete: Bool) {
            try! realm.write {
                self.todo.complete = complete
            }
        }
    }
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.darkGray
        label.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        return label
    }()
    
    lazy var checkbox: AIFlatSwitch = {
        let box = AIFlatSwitch(frame: .zero)
        box.strokeColor = .gray
        box.trailStrokeColor = .blue
        box.lineWidth = 3.0
        box.addTarget(self, action: #selector(completedChanged), for: .valueChanged)
        return box
    }()
    
    private lazy var stack: UIStackView = {
        let view = UIStackView(arrangedSubviews: [self.checkbox, self.nameLabel])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        view.alignment = .center
        view.spacing = 8
        return view
    }()
    
    var viewModel: ViewModel? {
        didSet {
            self.nameLabel.text = self.viewModel?.name
            self.checkbox.isSelected = self.viewModel?.complete == true
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }

    private func initialize() {
        self.selectionStyle = .none
        self.wrapper.addSubview(self.stack)
        self.stack.fillContainer(8)
        self.checkbox.height(32).heightEqualsWidth()
    }
    
    @objc func completedChanged() {
        self.viewModel?.set(complete: self.checkbox.isSelected)
    }
}
