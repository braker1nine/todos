//
//  ListCell.swift
//  Todo
//
//  Created by Chris Brakebill on 1/12/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import Stevia

class RoundedCell : UITableViewCell {
    
    lazy var wrapper: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 4.0
        view.layer.shadowColor = CGColor(srgbRed: 0, green: 0, blue: 0, alpha: 1.0)
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.1
        view.backgroundColor = .white
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    
    private func initialize() {
        self.selectionStyle = .none
        self.contentView.addSubview(self.wrapper)
        self.wrapper.fillVertically(m: 4).fillHorizontally(m: 8)
    }
}
