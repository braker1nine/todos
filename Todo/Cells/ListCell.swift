//
//  ListCell.swift
//  Todo
//
//  Created by Chris Brakebill on 1/12/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import Stevia

class ListCell : RoundedCell {
    
    struct ViewModel {
        let title: String
        let todosCount: Int
    }
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .darkGray
        label.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        label.setContentHuggingPriority(.required, for: .horizontal)
        return label
    }()
    
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 24, weight: .thin)
        label.textAlignment = .right
        return label
    }()
    
    var viewModel: ViewModel? {
        didSet {
            self.nameLabel.text = self.viewModel?.title
            self.countLabel.text = "\(self.viewModel?.todosCount ?? 0)"
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    private func initialize() {
        self.wrapper.addSubview(self.nameLabel)
        self.wrapper.addSubview(self.countLabel)
        self.nameLabel.fillVertically(m: 8).leading(16)
        self.countLabel.fillVertically(m: 8).trailing(16)
        self.countLabel.Leading == self.nameLabel.Trailing + 8
    }
}
