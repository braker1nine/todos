//
//  InputCell.swift
//  Todo
//
//  Created by Chris Brakebill on 1/12/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import Stevia

protocol InputCellDelegate {
    func accept(_ input: String?) -> Bool
}

class InputCell : RoundedCell {
    
    struct ViewModel {
        let placeholder: String
    }
    
    lazy var input: UITextField = {
        let input = UITextField()
        input.translatesAutoresizingMaskIntoConstraints = false
        input.font = UIFont.systemFont(ofSize: 24, weight: .medium)
        input.delegate = self
        input.returnKeyType = .done
        return input
    }()

    private lazy var stackView: UIStackView = {
        let view = UIStackView(arrangedSubviews: [self.input])
        view.translatesAutoresizingMaskIntoConstraints = false
        view.axis = .horizontal
        return view
    }()
    
    var viewModel: ViewModel? {
        didSet {
            if let viewModel = self.viewModel {
                self.input.attributedPlaceholder = NSAttributedString(
                    string: viewModel.placeholder,
                    attributes: [
                        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 24, weight: .light)
                    ]
                )
            } else {
                self.input.placeholder = ""
            }
        }
    }
    
    var delegate: InputCellDelegate?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    private func initialize() {
        self.selectionStyle = .none
        self.wrapper.addSubview(self.stackView)
        self.stackView.fillVertically(m: 6.0).leading(24.0).trailing(6.0)
    }
}

extension InputCell : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if delegate?.accept(textField.text) == true {
            textField.resignFirstResponder()
            textField.text = ""
        }
        
        return true
    }
}
