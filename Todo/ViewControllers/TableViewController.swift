//
//  TableViewController.swift
//  Todo
//
//  Created by Chris Brakebill on 1/13/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import Stevia

class TableViewController : UIViewController {
    
    lazy var tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false;
        view.backgroundColor = .background
        view.rowHeight = 64
        view.separatorStyle = .none
        view.keyboardDismissMode = .onDrag
        return view
    }()
    
    override func loadView() {
        self.view = self.tableView
    }
    
    override func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidChange(_:)), name: UIResponder.keyboardDidChangeFrameNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func keyboardDidChange(_ note: Notification) {
        if let frame = (note.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            self.tableView.contentInset.bottom = frame.size.height
        }
    }
}
