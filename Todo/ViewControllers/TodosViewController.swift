//
//  TodosViewController.swift
//  Todo
//
//  Created by Chris Brakebill on 1/11/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class TodosViewController : TableViewController {
    
    init(with list: TodoList) {
        self.dataSource = TodosViewDataSource(list: list)
        super.init(nibName: nil, bundle: nil)
        self.dataSource.tableView = self.tableView
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    let dataSource: TodosViewDataSource
    
    private func initialize() {
        self.title = self.dataSource.list.name
        self.tableView.dataSource = self.dataSource
        self.dataSource.tableView = self.tableView
        
        self.tableView.register(InputCell.self, forCellReuseIdentifier: "InputCell")
        self.tableView.register(TodoCell.self, forCellReuseIdentifier: "TodoCell")
    }
}

extension TodosViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
}

class TodosViewDataSource : NSObject, UITableViewDataSource {
    let list: TodoList
    var tableView: UITableView? = nil
    
    init(list: TodoList, tableView: UITableView? = nil) {
        self.list = list
        self.tableView = tableView
        super.init()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let todo = list.todos[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "TodoCell", for: indexPath) as! TodoCell
            cell.viewModel = TodoCell.ViewModel(todo: todo)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath) as! InputCell
            cell.viewModel = InputCell.ViewModel(placeholder: "Add todo...")
            cell.delegate = self
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return list.todos.count
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let todo = self.list.todos[indexPath.row]
            try! realm.write {
                realm.delete(todo)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
}

extension TodosViewDataSource : InputCellDelegate {
    func accept(_ input: String?) -> Bool {
        if let input = input, input.count > 0 {
            let newTodo = Todo()
            newTodo.name = input
            newTodo.complete = false
            try! realm.write {
                realm.add(newTodo)
                self.list.todos.append(newTodo)
            }
            self.tableView?.reloadData()
            return true
        } else {
            return false
        }
    }
}
