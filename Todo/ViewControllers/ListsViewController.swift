//
//  ListsViewController.swift
//  Todo
//
//  Created by Chris Brakebill on 1/11/20.
//  Copyright © 2020 Chris Brakebill. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift


class ListsViewController : TableViewController {
    
    init() {
        super.init(nibName: nil, bundle: nil)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    let dataSource = ListsViewDataSource()
    
    private func initialize() {
        self.title = "Lists"
        self.dataSource.tableView = self.tableView
        self.tableView.backgroundColor = .background
        self.tableView.delegate = self
        self.tableView.dataSource = self.dataSource
        self.tableView.register(InputCell.self, forCellReuseIdentifier: "InputCell")
        self.tableView.register(ListCell.self, forCellReuseIdentifier: "ListCell")
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let selected = self.tableView.indexPathForSelectedRow {
            self.tableView.deselectRow(at: selected, animated: true)
            self.tableView.reloadRows(at: [selected], with: .automatic)
        }
    }
}

extension ListsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 0 {
            return indexPath
        }
        
        return nil
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let list = self.dataSource.lists[indexPath.row]
        let listVc = TodosViewController(with: list)
        self.navigationController?.pushViewController(listVc, animated: true)
    }
}

class ListsViewDataSource : NSObject, UITableViewDataSource {
    let lists = realm.objects(TodoList.self)
    var tableView: UITableView? = nil
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let list = lists[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
            cell.viewModel = ListCell.ViewModel(
                title: list.name,
                todosCount: list.todos.count
            )
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InputCell", for: indexPath) as! InputCell
            cell.viewModel = InputCell.ViewModel(placeholder: "New List...")
            cell.delegate = self
            return cell
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return lists.count
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let list = lists[indexPath.row]
            try! realm.write {
                realm.delete(list)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 0
    }
}

extension ListsViewDataSource : InputCellDelegate {
    func accept(_ input: String?) -> Bool {
        if let input = input, input.count > 0 {
            let newList = TodoList()
            newList.name = input
            try! realm.write {
                realm.add(newList)
            }
            self.tableView?.reloadData()
            return true
        } else {
            return false
        }
    }
}
